import pickle


RELATIVE_MODELS_DIR = "../../models/"

def save_model(clf, model_type, model_id):
    with open("{}{}_{}.pkl".format(RELATIVE_MODELS_DIR, model_type, model_id), 'wb') as f:
        pickle.dump(clf, f)


def load_model(model_type, model_id):
    with open("{}{}_{}.pkl".format(RELATIVE_MODELS_DIR, model_type, model_id), 'rb') as f:
        return pickle.load(f)