import os

import pandas as pd

def load_data():
    """
    Loads the data assuming it is in the 'docs' dir in the root of the directory.
    Will search that location (relative).

    No params are required, considering this assumption.
    """
    dir_relative_path = os.path.join("..", "..", "docs")
    filename = "DatasetML.csv"
    full_filename = os.path.join(dir_relative_path, filename)

    return pd.read_csv(full_filename)