import numpy as np

def compute_basic_stats(data):
    mean = np.nanmean(data)
    stdev = np.nanstd(data)
    min = np.min(data)
    q1 = np.percentile(data, 25)
    median = np.percentile(data, 50)
    q3 = np.percentile(data, 75)
    max = np.max(data)
    iqr = q3-q1
    count = len(data)

    return mean, stdev, min, q1, median, q3, max, iqr, count