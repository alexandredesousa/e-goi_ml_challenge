from pandas.api.types import infer_dtype
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from src.utils import load_data
from src.preliminary_analysis.stats import compute_basic_stats

# see:
# https://realpython.com/python-data-cleaning-numpy-pandas/
# https://towardsdatascience.com/data-cleaning-with-python-using-pandas-library-c6f4a68ea8eb
# https://towardsdatascience.com/data-cleaning-using-python-pandas-f6fadc433535
# https://towardsdatascience.com/data-cleaning-in-python-the-ultimate-guide-2020-c63b88bf0a0d

df = load_data.load_data()
col_names = df.columns

# Get a grasp of the data
print(df.head)
print(df.info())
print(len(col_names))

# types of variables
print(col_names)
print([infer_dtype(df[col_name]) for col_name in col_names])

# NAs
print(df.isnull().sum())

## visually
colours = ['#000099', '#ffff00']
sns.heatmap(df[col_names].isnull(), cmap=sns.color_palette(colours))
plt.show()

for col in col_names:
   pct_missing = np.mean(df[col].isnull())
   print(round(pct_missing))

# evaluate possible categorical variables, apparently numerical
numeric_col_names = [name for name in col_names.tolist() if name.startswith('N')]
print([df[col_name].nunique() for col_name in numeric_col_names])
# or, more general method:
numeric_cols = df.select_dtypes(include=[np.number]).columns.values

cat_col_names = [name for name in col_names.tolist() if name.startswith('C')]
print([df[col_name].nunique() for col_name in cat_col_names])
# or, more general method:
non_numeric_cols = df.select_dtypes(include=[np.number]).columns.values

# duplicate values
# rows
print(df.duplicated().sum())

# columns
print(df.T.duplicated().sum())

# Outlier analysis (considering numerical vars: N1, N2, N5
actual_numeric_cols = ['N1', 'N2', 'N5']
for anc in actual_numeric_cols:
    print(df[anc].describe())

n_bins = np.sqrt(df.shape[0]).astype(int)
for anc in actual_numeric_cols:
    # each in separate plots
    df[anc].hist(bins=n_bins)
    df.boxplot(column=[anc])

    # both in the same plot
    # see https://www.python-graph-gallery.com/24-histogram-with-a-boxplot-on-top-seaborn

    # sets theme - grey background; bright color
    sns.set_theme(style="darkgrid", palette='bright')

    # creates figures composed of two subplots (ax_box and ax_hist)
    f, (ax_box, ax_hist) = plt.subplots(2, sharex=True)

    # assigns graph to each ax subplot
    sns.boxplot(df[anc], ax=ax_box)
    sns.histplot(data=df, x=anc, ax=ax_hist)

    # adds title to the fig and removes x axis label for the boxplot (it is the same as the hist, and avoids repetition)
    f.suptitle('Boxplot & histogram for var {}'.format(anc))
    ax_box.set(xlabel='')

    plt.show()

for anc in actual_numeric_cols:
    mean, stdev, min, q1, median, q3, max, iqr, count = compute_basic_stats(df[anc])

    # for potential outlier evaluation
    # compute value of mean + 3 sigmas
    mean_3sigma = mean + 3*stdev

    # compute value o superior limit for boxplot (usually, as well as seaborn, whiskers uses the constant 1.5
    top_whisker = q3 + 1.5*iqr

    print("Var {}. mean_3sigma={} ".format(anc, mean_3sigma))
    print("Var {}. top_whisker={} ".format(anc, top_whisker))

    # records above value:
    get_above = len(df[df[anc] > top_whisker])
    print("Var {}. above top whisker value: {} (percent: {})".format(anc, get_above, get_above/count))

# frequency of the target variable
print(df.groupby(['LABEL']).count())