from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd

from src.modelling import feature_engineering, Classifiers, evaluation
from src.utils import load_data, save_models

# load data
df = load_data.load_data()

X = df.iloc[:, :-1]
y = df.iloc[:, -1]

# must perform split previous to any feature engineering - data leakage
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=20210313)
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=20210313, shuffle=True)

# convert label to type category. should not have impact, but still these are category labels.
y_train = y_train.astype("category")
y_test = y_test.astype("category")
engineered_data = feature_engineering.FeatureEngineering(X_train, X_test)
# engineered_data = feature_engineering.FeatureEngineering(X_train, X_test, scaler=feature_engineering.Scaler('robust').scaler)

# perform default pre-processing
X_train_proc, X_test_proc = engineered_data.preprocessing()

grid_params = {
    "kernel": ["rbf", "sigmoid", "linear", "poly"],
    "C": [1, 2, 4, 8, 16, 32, 100],
    "gamma": np.logspace(-6, 6, 10)
}

clf = Classifiers.Model('svm', grid_params=grid_params)
clf.grid_search(X_train_proc, y_train, grid_params)

score = clf.fit_clf.score(X_test_proc, y_test)

print("Improved svm model Score: {}".format(score))
print("Best params: {}".format(clf.best_params))

acc_1, prec_1, rec_1, f1s_1 = evaluation.get_metrics(y_test, clf.fit_clf.predict(X_test_proc))
print("Model: grid_svr; Accuracy: {}, Precision: {}, Recall: {}, F1-score: {}".format(acc_1, prec_1, rec_1, f1s_1))

save_models.save_model(clf.fit_clf, 'grid', 'svr')

# # if you want to load baseline models:
# loaded_clf = save_models.load_model('grid', 'svr')