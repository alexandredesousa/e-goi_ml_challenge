from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, confusion_matrix
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt


def get_confusion_matrix(y_pred, y_true, labels):
    cm = confusion_matrix(y_true, y_pred, labels=labels)
    df_cm = pd.DataFrame(cm, index=reversed(labels), columns=labels)

    sns.heatmap(df_cm, annot=True, fmt='d')

def get_metrics(y_true, y_pred):
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1s = f1_score(y_true, y_pred)
    # roc_auc = roc_auc_score(y_true, y_pred)

    return accuracy, precision, recall, f1s
