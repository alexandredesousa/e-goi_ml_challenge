from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
import pandas as pd
import numpy as np

class FeatureEngineering:
    """
    Object to store and process data related with preprocessing.

    At initialization receives train and test features for transformation.
    Considering that some numerical variables are thought to actually be categorical,
    there is also the parameter num_cat, which should be used as indication if these variables
    are to be considered categorical (num_cat=True) or as numeric (num_cat=False).

    The pre-processing performed in this class is such that:
    - There are two pipelines - self.categorical_transformer and self.numerical_transformer -
    one for each data type, i.e. categorical variables and numerical variables.
    - The pre-processing of the variables is performed in a ColumnTransformer - self.preprocessor -
    that applies each pipeline according to the columns variable types.

    The pipelines can be freely extended with more steps with no further changes to the remainder of the code.
    Currently there are the following transformations in the pipelines:
    - Pre-processing ops for numerical variables:
        - For scalling of the numeric variables, the class Scaler is used.
        - Different types of scalers are defined there, and more can be added as desired.
        - By default, (i.e. if optional parameter 'scaler' is not passed) uses scikit StandardScaler().
    - Pre-processing ops for categorical variables:
        - One-hot encoder is used to transform the categorical variables.

    At initialization the properties are only set up.
    It was decided that the actual preprocessing (or data transformation) is controlled by the class user
    by calling the 'preprocessing' routine.

    Both the original features (self.train_features and self.test_features) and
    the transformed (or encoded) features (self.X_train_enc and self.X_test_enc) are kept.
    The class user could access the properties self.X_train_enc and self.X_test_enc after calling the
    preprocessing routine, however, for agility, the routine also returns those values, in case the user
    intends to use them after call.

    see also:
    # https://scikit-learn.org/stable/modules/preprocessing.html

    pipelines:
    https://adhikary.net/en/2019/03/23/categorical-and-numeric-data-in-scikit-learn-pipelines/
    https://johaupt.github.io/scikit-learn/tutorial/python/data%20processing/ml%20pipeline/sklearn_categorical_variables_pipeline.html

    encoding transformations:
    https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OneHotEncoder.html
    https://stackoverflow.com/questions/56556148/why-columntransformer-does-not-call-fit-on-its-transformers/56556482
    """

    def __init__(self, train_features, test_features, num_cat=True, scaler=None):
        self.train_features = train_features
        self.test_features = test_features

        # info related to variable types, i.e. categorical or numerical could (or should?) be received as parameter.
        # at the moment, controlled by the boolean num_cat
        self.num_cat = num_cat                          # suspicious numeric variables to consider as categorical?

        self.num_as_cat = ['N3', 'N4', 'N6', 'N7']      # suspicious numeric variables

        # numerical variables
        self.num_vars = ['N1', 'N2', 'N5']              # if not num_cat else ['N1', 'N2', 'N5'] + self.num_as_cat

        # categorical variables:
        self.base_cat_vars = ['C1', 'C2', 'C3', 'C4', 'C5', 'C5.1', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12']
        self.cat_vars = self.base_cat_vars + self.num_as_cat # if num_cat else self.base_cat_vars

        for cat in self.num_as_cat:
            train_features[cat] = train_features[cat].astype(str)
            test_features[cat] = test_features[cat].astype(str)

        ## pre-processing properties
        # scaler for the numerical variables
        if scaler is None:
            self.num_scaler = Scaler().scaler
        else:
            self.num_scaler = scaler

        # encoder for the categorical variables
        # if sparse = True returns a sparse matrix. would need to use .toarray to convert to a numpy array.
        # as we are not going to work with sparse matrix for this, save the next step of calling .toarray() and
        # receive a numpy array with the encode.
        self.cat_encoder = preprocessing.OneHotEncoder(handle_unknown='ignore', sparse=False)
        # self.cat_encoder = preprocessing.OneHotEncoder(handle_unknown='error', sparse=False, drop="first")

        # numerical and categorical transformers
        self.numerical_transformer = Pipeline(steps=[
            ('scaler', self.num_scaler)
        ])
        self.categorical_transformer = Pipeline(steps=[
            ('onehot', self.cat_encoder)
        ])

        self.preprocessor = ColumnTransformer(
            transformers=[
                ('num', self.numerical_transformer, self.num_vars),
                ('cat', self.categorical_transformer, self.cat_vars)
            ],
            remainder='drop'
        )

        # only training data is used to fit the pre-processing transformer
        self.preprocessor.fit(self.train_features)

        # after the fitting process the variable names are lost. recover:
        self.fit_preproc_categories = self.preprocessor.named_transformers_['cat'].named_steps['onehot'].get_feature_names()
        self.fit_preproc_vars = np.append(self.num_vars, self.fit_preproc_categories)

    def preprocessing(self):
        """
        Performs the transformation of the training and testing features using the already fit self.preprocessor.
        Both the non-encoded and the encoded versions are kept.
        non-encoded - self.train_features and self.test_features
        encoded - self.X_train_enc and self.X_test_enc

        The user of the class can access the encoded values as properties of the class, however, as the routine is used
        to perform the processing, for agility they are also passed as return.

        :return: encoded version as a pandas data frame
        """
        self.X_train_enc = pd.DataFrame(self.preprocessor.transform(self.train_features), columns=self.fit_preproc_vars)
        self.X_test_enc = pd.DataFrame(self.preprocessor.transform(self.test_features), columns=self.fit_preproc_vars)

        return self.X_train_enc, self.X_test_enc


class Scaler:

    def __init__(self, scaler_str=None):
        if scaler_str is None:  # default scaler used
            self.scaler = preprocessing.StandardScaler()
        elif scaler_str == "minmax":
            self.scaler = preprocessing.MinMaxScaler()
        elif scaler_str == "abs":
            self.scaler = preprocessing.MaxAbsScaler()
        elif scaler_str == "robust":
            self.scaler = preprocessing.RobustScaler() # scaling with outliers

    def scale(self, df, columns=None):
        """
        Perform the scaling operation of the data in df, using the scaler object.
        The column names in a pandas data frame are lost after scaling (or rescaling) operation,
        as such, either the column names are provided as a parameter, or the current column names
        are used and the data frame is returned with the same column names.

        :param df: data to be scaled
        :param columns: used in case one wants to rename the data frame columns after scaling
        :return: scaled data
        """
        if columns is None:
            columns = df.columns.values
        return pd.DataFrame(self.scaler.fit_transform(df), columns)

    def rescale(self, df, columns=None):
        """
        Perform the rescaling operation of the data in df, using the scaler object.
        The column names in a pandas data frame are lost after scaling (or rescaling) operation,
        as such, either the column names are provided as a parameter, or the current column names
        are used and the data frame is returned with the same column names.

        :param df: data to be rescaled
        :param columns: used in case one wants to rename the data frame columns after rescaling
        :return: rescaled data
        """
        if columns is None:
            columns = df.columns.values
        return pd.DataFrame(self.scaler.inverse_transform(df), columns=columns)
