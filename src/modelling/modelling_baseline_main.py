from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd

from src.modelling import feature_engineering, Classifiers

from src.utils import load_data, save_models

# load data
df = load_data.load_data()

X = df.iloc[:, :-1]
y = df.iloc[:, -1]

# must perform split previous to any feature engineering - data leakage
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=20210313)

# convert label to type category. should not have impact, but still these are category labels.
y_train = y_train.astype("category")
y_test = y_test.astype("category")
engineered_data = feature_engineering.FeatureEngineering(X_train, X_test)

# perform default pre-processing
X_train_proc, X_test_proc = engineered_data.preprocessing()

# set baseline models (default parameters)
base_clfs = [member.value for name, member in Classifiers.Classifiers_sk.__members__.items()]

# iterate over classifiers
baseline_scores = []    # mean accuracy
for clf in base_clfs:
    clf.fit(X_train_proc, y_train)
    baseline_scores.append(clf.score(X_test_proc, y_test))

base_clfs_name = [member.name for name, member in Classifiers.Classifiers_sk.__members__.items()]

for name, score, clf in zip(base_clfs_name, baseline_scores, base_clfs):
    print("Model: {}; Score: {}".format(name, score))
    # save_models.save_model(clf, 'baseline', name)

# # if you want to load baseline models:
# loaded_clf = save_models.load_model('baseline', 'dt')
# print(loaded_clf.score(X_test_proc, y_test))