from enum import Enum

from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.model_selection import GridSearchCV

class Classifiers_sk(Enum):
    """
    Enum for implemented classifiers using sklearn.

    Will implement:
    svm - Support Vector Machine classifier
    dt - Decision Tree Classifier
    lr - Logistic Regression Classifier
    knn - K-Nearest Neighbors Classifier
    nb - Naive Bayes Classifier
    e_rf - Random Forest Classifier
    e_ada - Ada Boost Classifier

    see:
    https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html
    """

    svm = SVC()
    dt = DecisionTreeClassifier()
    lr = LogisticRegression()
    knn = KNeighborsClassifier()
    nb = GaussianNB()
    e_rf = RandomForestClassifier()
    e_ada = AdaBoostClassifier()


class Model:
    def __init__(self, classifier_str, model_params=None, grid_params=None):
        self.clf_str = classifier_str
        self.model_params = model_params
        self.grid_params = grid_params
        for name, member in Classifiers_sk.__members__.items():
            if name == self.clf_str:
                self.clf = member.value

    def grid_search(self, train_features, train_target, grid_params=None):

        self.grid_params = grid_params

        # settings
        grid = GridSearchCV(self.clf, self.grid_params, cv=5, verbose=0, n_jobs=-1)

        # model train
        self.fit_clf = grid.fit(train_features, train_target)

        self.best_params = grid.best_params_
        self.best_estimator = grid.best_estimator_

        return self.best_params, self.best_estimator




