# Description
Project to develop for the E-goi Machine Learning Engineer challenge

# Due
Sunday, 14th March, 2021

# Python version
Used a python venv, with version:
```
(venv) MacBook-Pro-de-Alexandre:e-goi_ml_challenge alexandre.c.sousa$ python --version
Python 3.7.9
```

# Python packages used
The following packages are required:
```
(venv) MacBook-Pro-de-Alexandre:e-goi_ml_challenge alexandre.c.sousa$ pip list
Package         Version
--------------- -------
cycler          0.10.0
joblib          1.0.1
kiwisolver      1.3.1
matplotlib      3.3.4
numpy           1.20.1
pandas          1.2.3
Pillow          8.1.2
pip             21.0.1
pyparsing       2.4.7
python-dateutil 2.8.1
pytz            2021.1
scikit-learn    0.24.1
scipy           1.6.1
seaborn         0.11.1
setuptools      47.1.0
six             1.15.0
threadpoolctl   2.1.0
```
