This document concerns the wotk to provide some baseline results.

To that aim, implemented:
- [feature_engineering.py](../../src/modelling/feature_engineering.py)
- [Classifiers](../../src/modelling/Classifiers.py)
- [modelling_baseline_main](../../src/modelling/modelling_baseline_main.py)

Assumptions for baseline results:
- **Feature engineering**:
  - Implements some pre-processing pipelines that are used to perform a basic transformation of data in order
  to allow for the use in some of the classifiers used.
  - For numerical variables, a scaler is used in order for all numerical variables to be of the same order of magnitude.
  - For categorical variables, one-hot encoder is used in order to transform the categories.
- **Classifiers**:
   - Implements some scikit-lern classifiers, specifically: Support Vector Machine, Decision Tree, Logistic Regression,
   K-Nearest Neighbors, Naive Bayes, Random Forest and AdaBoost Classifiers.
   - Default parameter values were used at this stage.

The baseline results are:
```
Model: svm; Score: 0.7696969696969697
Model: dt; Score: 0.6787878787878788
Model: lr; Score: 0.7424242424242424
Model: knn; Score: 0.7424242424242424
Model: nb; Score: 0.6545454545454545
Model: e_rf; Score: 0.7696969696969697
Model: e_ada; Score: 0.7515151515151515
```

The [modelling_baseline_main](../../src/modelling/modelling_baseline_main.py) script
can be used to replicate the results.

Considering the results, will **further explore the SVM classifiers** in the future stages.